package ru.tsc.denisturovsky.tm;

import ru.tsc.denisturovsky.tm.constant.TerminalArgument;
import ru.tsc.denisturovsky.tm.constant.TerminalCommand;

import java.util.Locale;
import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        if (processArgument(args)) System.exit(0);
        Scanner scanner = new Scanner(System.in);
        showWelcome();
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalArgument.ABOUT:
                showAbout();
                break;
            case TerminalArgument.HELP:
                showHelp();
                break;
            case TerminalArgument.VERSION:
                showVersion();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalCommand.ABOUT:
                showAbout();
                break;
            case TerminalCommand.HELP:
                showHelp();
                break;
            case TerminalCommand.VERSION:
                showVersion();
                break;
            case TerminalCommand.EXIT:
                close();
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static void close() {
        System.exit(0);
    }

    public static void showErrorCommand(final String command) {
        System.err.format("[Error] \n");
        System.err.format("This command '%s' not supported \n", command);
    }

    public static void showErrorArgument(final String arg) {
        System.err.format("[Error] \n");
        System.err.format("This argument '%s' not supported \n", arg);
    }

    public static void showVersion() {
        System.out.format("[%s] \n", TerminalCommand.VERSION.toUpperCase(Locale.ROOT));
        System.out.format("1.6.0 \n");
    }

    public static void showHelp() {
        System.out.format("[%s] \n", TerminalCommand.HELP.toUpperCase(Locale.ROOT));
        System.out.format("%s, %s - Show developer info \n", TerminalCommand.ABOUT, TerminalArgument.ABOUT);
        System.out.format("%s, %s - Show list of terminal commands \n", TerminalCommand.HELP, TerminalArgument.HELP);
        System.out.format("%s, %s - Show program version \n", TerminalCommand.VERSION, TerminalArgument.VERSION);
        System.out.format("%s - Close application \n", TerminalCommand.EXIT);
    }

    public static void showAbout() {
        System.out.format("[%s] \n", TerminalCommand.ABOUT.toUpperCase(Locale.ROOT));
        System.out.format("Name: %s %s \n", TerminalCommand.FIRST_NAME, TerminalCommand.LAST_NAME);
        System.out.format("E-mail: %s \n", TerminalCommand.EMAIL);
    }

    public static void showWelcome() {
        System.out.format("%s \n", TerminalCommand.WELCOME);
    }

}
